from django.test import SimpleTestCase
from django.urls import reverse, resolve
from blog.views import all_blogs, detail

class BlogUrlsTest(SimpleTestCase):
    def test_all_blogs_url_resolves(self):
        url = reverse('blog:all_blogs')
        self.assertEqual(resolve(url).func, all_blogs)
    
    def test_detail_url_resolves(self):
        blog_id = 1  # Предположим, что у вас есть существующий блог с id=1
        url = reverse('blog:detail', args=[blog_id])
        self.assertEqual(resolve(url).func, detail)
