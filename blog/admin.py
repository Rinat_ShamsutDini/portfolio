from django.contrib import admin
from .models import Project

class ProjectModelAdmin(admin.ModelAdmin):
    list_display = ("id", "title")

admin.site.register(Project, ProjectModelAdmin)

#admin.site.register(Project)
